package com.vistarooms.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.vistarooms.main.DriverFactory;

public class VistaHomePage {
	
	WebDriver driver = DriverFactory.getDriver();
	
	@FindBy(xpath = "//a[text()='Login']")
	WebElement login;
	
	@FindBy(xpath = "//a[text()='signup']")
	WebElement signiup;
	
	
	public void loginClick() {
		login.click();
	}
	
	public void signupClick() {
		signiup.click();
	}
	
	public VistaHomePage() {
		PageFactory.initElements(driver, this);
	}

}
