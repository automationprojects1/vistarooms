package com.vistarooms.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.vistarooms.main.DriverFactory;

public class LoginPage {

	WebDriver driver = DriverFactory.getDriver();
	
	//private static final String LOGIN_PAGE_ROOT="//div[@class='m-login__contanier']";
	
	@FindBy(xpath = "//a[text()='More login options']")
	public WebElement loginoption;

	@FindBy(xpath = "//a[text()='Sign in using Facebook']")
	public WebElement facebooklogin;

	@FindBy(xpath = "//a[text()=' Login in with google']")
	public WebElement googlelogin;

	@FindBy(xpath = "//input[@id='login_email']")
	private WebElement user_emailid;

	@FindBy(xpath = "//input[@id='login_password']")
	private WebElement user_password;

	@FindBy(xpath = "//a[@id='custLogin']")
	private WebElement loginbtn;

	@FindBy(xpath = "//input[@id='login_email']")
	public WebElement forgotpassword;

	@FindBy(xpath = "//input[@id='login_email']")
	public WebElement rememberme;

	@FindBy(xpath = "//input[@id='normlSignup']")
	public WebElement signuponloginform;

	public void ClickOnLoginOption() {
		loginoption.click();
	}

	public void LoginWithFacebook() {
		facebooklogin.click();
	}

	public void LoginWithGoogle() {
		googlelogin.click();
	}

	public void ActionOnLoginPage(String username, String password) {
		user_emailid.sendKeys(username);
		user_password.sendKeys(password);
		loginbtn.click();
	}

	public void ClickOnFrgotPasswordLink() {
		forgotpassword.click();
	}

	public void CheckRememberMe() {
		rememberme.click();
	}

	public void CheckSignupBtnLink() {
		signuponloginform.click();
	}

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

}
