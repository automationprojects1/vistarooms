package com.vistarooms.main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import com.vistarooms.utils.GenericUtils;

public class BaseTest extends GenericUtils {

	WebDriver driver = DriverFactory.getDriver();

	@BeforeMethod
	public void setUp() {
		// URL also comes from properties file
		driver.get("https://www.vistarooms.com/");
		driver.manage().window().maximize();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	
	
	public static String getProperty(String property) {
		InputStream input;
		String requiredProperty = null;
		try {
			input = new FileInputStream("resources/config.properties");
			Properties prop = new Properties();
			prop.load(input);
			requiredProperty = prop.getProperty(property);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return requiredProperty;
	}
	
	//Code for Extend report
	 public static ExtentHtmlReporter htmlReporter;
	    public static ExtentReports extent;
	    public static ExtentTest test;
	     
	    @BeforeSuite
	    public void ReportsetUp()
	    {
	        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/MyOwnReport.html");
	        extent = new ExtentReports();
	        extent.attachReporter(htmlReporter);
	         
	        extent.setSystemInfo("OS", "Windows 10");
	        extent.setSystemInfo("Host Name", "Vista");
	        extent.setSystemInfo("Environment", "QA");
	        extent.setSystemInfo("User Name", "Gunjan Jain");
	         
	       // htmlReporter.config().setChartVisibilityOnOpen(true);
	        htmlReporter.config().setDocumentTitle("AutomationTesting.in TestRun Report");
	        htmlReporter.config().setReportName("My Own Report");
	      // htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
	       // htmlReporter.config().setTheme(Theme.DARK);
	    }
	     
	    @AfterMethod
	    public void getResult(ITestResult result)
	    {
	        if(result.getStatus() == ITestResult.FAILURE)
	        {
	            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
	            test.fail(result.getThrowable());
	        }
	        else if(result.getStatus() == ITestResult.SUCCESS)
	        {
	            test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
	        }
	        else
	        {
	            test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
	            test.skip(result.getThrowable());
	        }
	    }
	     
	    @AfterSuite
	    public void tearDownAtLast()
	    {
	        extent.flush();
	    }
}
