package com.vistarooms.test;

import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.vistarooms.main.BaseTest;
import com.vistarooms.page.LoginPage;
import com.vistarooms.page.VistaHomePage;
import com.vistarooms.utils.ExcelUtils;

@Listeners(com.vistarooms.test.ListenerClass.class)
public class LoginTest extends BaseTest {

	LoginPage loginpage = new LoginPage();
	VistaHomePage homepage = new VistaHomePage();

	@BeforeClass
	public void setUp() {
		super.setUp();
		super.waitForPageLoad();
	}

	@DataProvider
	public Object[][] getTestCaseData(Method testMethod) {
		return ExcelUtils.getTestcaseAllData(testMethod.getName(), super.getProperty("loginsheet"),
				super.getProperty("datatable"));
	}

	@SuppressWarnings("static-access")
	@Test(dataProvider = "getTestCaseData", priority = 1)
	public void loginWithValidCredentials(String username, String password) throws InterruptedException {
		test = extent.createTest("login To Vistarooms With Valid Credentials");
		Thread.sleep(10000);
		homepage.loginClick();
		loginpage.ClickOnLoginOption();
		loginpage.ActionOnLoginPage(username, password);

	}

	@Test(dataProvider = "getTestCaseData", priority = 2)
	public void loginWithInvalidCredentials(String username, String password) throws InterruptedException {
		test = extent.createTest("login with invalid credentials");
		Thread.sleep(5000);
		homepage.loginClick();
		loginpage.ClickOnLoginOption();
		loginpage.ActionOnLoginPage(username, password);
	}

	@AfterClass
	public void tearDown() {
		super.tearDown();
	}
}
